const index = require("./index")
const Benchmark = require("benchmark")

const suite = new Benchmark.Suite;

const numbers = [...Array(400)].map(e=>Math.random()*40|0)
// add tests
suite.add("Higher order", function () {
    index.countBoomerangs2(numbers);
}).add("For loop", function () {
    index.countBoomerangs(numbers)
})
    // add listeners
    .on('cycle', function (event) {
        console.log(String(event.target));
    })
    .on('complete', function () {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    // run async
    .run({'async': true});