function countBoomerangs(a){
    let boom = 0
    for (let i = 0; i < a.length-2; i++) {
        if (a[i] === a[i+2] && a[i] !== a[i+1]){
            boom++
        }
    }
    return boom
}


function countBoomerangs2(a){
    return a.filter((n,i) => (n===a[i+2] && n!==a[i+1])).length
}

function countBoomerangs3(a){
    let boom = 0
    const l = Math.floor(a.length / 2)
    for (let i = 1; i < a.length-2; i++) {
        if (a[i] === a[i+2] && a[i] !== a[i+1]){
            boom++
        }
    }
    return boom
}



module.exports.countBoomerangs = countBoomerangs
module.exports.countBoomerangs2 = countBoomerangs2