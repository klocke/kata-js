const boomerang = require("./index.js")

it('should return 2', function () {
    const arr = [9,5,9,5,1,1,1];

    expect(boomerang.countBoomerangs(arr)).toBe(2);
});

it('should return 1', function () {
    const arr = [5, 6, 6, 7, 6, 3, 9];

    expect(boomerang.countBoomerangs(arr)).toBe(1);
});


it('should return 0', function () {
    const arr = [4, 4, 4, 9, 9, 9, 9];

    expect(boomerang.countBoomerangs(arr)).toBe(0);
});

it('should return 5', function () {
    const arr = [1, 7, 1, 7, 1, 7 ,1];

    expect(boomerang.countBoomerangs(arr)).toBe(5);
});