
module.exports.pigLatinSentence = function (sentence) {
    const vowels = /[aeiou]/;
    const newList = []
    for (const word of sentence.split(' ')){
        if (vowels.test(word[0])){
            newList.push(word + "way")
        }
        else {
            let i =1;
            while (!vowels.test(word[i])){
                i++
            }

            newList.push(
                word.slice(i) + word.slice(0, i) + "ay"
            )
        }
    }
    return newList.reduce((a, b) => a +' ' + b);t
}


module.exports.pigLatinSentenceHigherOrder = function (sentence) {
    return sentence.split(' ') // split sentence to list of words
        .map((word) => [word, word.match(/[aeiou]/).index]) // map to [word, index of first vowel]
        .map(([w, vowelIdx]) => vowelIdx === 0? // map each [word, index] to correct piglatin
                w + "way" : // first letter vowel case
                w.slice(vowelIdx) + w.slice(0, vowelIdx) + "ay")
        .reduce((a, b) => a + ' ' + b ) // concatenate the array of piglatin words to sentence
}