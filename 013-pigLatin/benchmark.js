

const {pigLatinSentence, pigLatinSentenceHigherOrder} = require("./index")
const {Suite} = require("benchmark")

const suite = new Suite;



// add tests
suite.add("Piglatin For", function () {
    pigLatinSentence("i found an ounce with my hound");
}).add("Piglatin higher order", function () {
    pigLatinSentenceHigherOrder("i found an ounce with my hound");
})
    // add listeners
    .on('cycle', function (event) {
        console.log(String(event.target));
    })
    .on('complete', function () {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    // run async
    .run({'async': true});