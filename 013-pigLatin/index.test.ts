const pigLatinSentence = require("./index.js").pigLatinSentenceHigherOrder
import theoretically from "jest-theories";


describe("pigLatin", function () {
    const theories = [
        {s:"this is pig latin", expected:"isthay isway igpay atinlay"},
        {s:"wall street journal", expected:"allway eetstray ournaljay"},
        {s:"raise the bridge", expected:"aiseray ethay idgebray"},
        {s:"all pigs oink", expected:"allway igspay oinkway"},
    ]

    theoretically(`{input} returns {expected}`, theories, theory =>  {
        const output = pigLatinSentence(theory.s);
        expect(output).toBe(theory.expected);
    })
})