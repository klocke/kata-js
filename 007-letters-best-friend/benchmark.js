// import bestFriend from "./index";

const bestFriend = require("./index")
const Benchmark = require("benchmark")

const suite = new Benchmark.Suite;

// add tests
suite.add("kk", function () {
    bestFriend.kk("i found an ounce with my hound", "o", "u");
}).add("benjamin", function () {
    bestFriend.benj("i found an ounce with my hound", "o", "u")
})
    // add listeners
    .on('cycle', function (event) {
        console.log(String(event.target));
    })
    .on('complete', function () {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    // run async
    .run({'async': true});