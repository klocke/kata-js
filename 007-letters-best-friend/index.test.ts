
const bestFriend = require("index.js").kk;


it('should let h and e be best friends', function () {
    expect(bestFriend("he headed to the store", "h", "e")).toBe(true)
});

it('should let o and u be best friends', function () {
    expect(bestFriend("i found an ounce with my hound", "o", "u")).toBe(true)
});

it('should not let h and e be best friends', function () {
    expect(bestFriend("we found your dynamite", "d", "y")).toBe(false)
});