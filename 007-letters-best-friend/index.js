function bestFriend_kk(sentence, first, second){
    return [...sentence].every((c, i) => c === first ? sentence[i+1] === second : true);
}

function bestFriend_benj(s, l1, l2){
    for (let i = 0; i < s.length; i++) {
        if (s.charAt(i) === l1) {
            if (s.charAt(i+1) !== l2){
                return false;
            }
        }
    }
    return true;
}



module.exports.kk = bestFriend_kk
module.exports.benj= bestFriend_benj