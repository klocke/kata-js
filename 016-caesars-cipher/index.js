function caesarCipher(s, n){
    return [...s].map( (c, i) =>
        /[a-z]/.test(c) ?
            String.fromCharCode(97 + (s.charCodeAt(i) + n-97)%26) :
            /[A-Z]/.test(c)?
                String.fromCharCode(65 + (s.charCodeAt(i) + n-65)%26)
                : c )
        .reduce((p, c) => p+c);
}

function caesarSalad(s, n) {
    s.charset
}


module.exports.caesarCipher = caesarCipher