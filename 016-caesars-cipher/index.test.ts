import {caesarCipher} from "./index.js"




import theoretically from "jest-theories";


describe("ceasarCipher", function () {
    const theories = [
        {s: "middle-Outz", n:2, expected: "okffng-Qwvb"},
        {s:"Always-Look-on-the-Bright-Side-of-Life", n:5, expected:"Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"},
        {s:"A friend in need is a friend indeed", n:20, expected:"U zlcyhx ch hyyx cm u zlcyhx chxyyx"},
        {s:"This-sentence should-not be-altered", n:0, expected:"This-sentence should-not be-altered"}
    ]

    theoretically(`{input} returns {expected}`, theories, theory =>  {
        const output = caesarCipher(theory.s, theory.n);
        expect(output).toBe(theory.expected);
    })
})