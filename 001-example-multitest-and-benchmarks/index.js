function add(a, b){
    return a + b
}

function add2(a, b){
    // a possibly slower method that does the same as add
    
    (async () => await new Promise(resolve => setTimeout(resolve, 200)))(); // do work for 200ms
    return a + b

}

module.exports.add = add;
module.exports.addSlow = add2;
