const add = require("./index.js").add

// REQUIRES npm install jest-theories
// RUN tests with multiple data :) 

import theoretically from "jest-theories";
​
​
describe("add", function () {
    const theories = [
        {a:1, b:1, expected:2},
        {a:0, b:1, expected:1},
        {a:1, b:0, expected:1},
    ]
​
    theoretically(`{input} returns {expected}`, theories, theory =>  {
        const output = add(theory.a, theory.b);
        expect(output).toBe(theory.expected);
    })
})


