/*

REQUIRES npm install benchmark

to run:

node <foldername>/benchmark.js

*/



const index = require("./index")
const Benchmark = require("benchmark")

const suite = new Benchmark.Suite;


// data to be passed to benchmark
const a = 0; 
const b = 0;


// add tests
suite.add("Add", function () {
    index.add(numbers);
}).add("Add 2", function () {
    index.addSlow(numbers)
})
    // add listeners
    .on('cycle', function (event) {
        console.log(String(event.target));
    })
    .on('complete', function () {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    // run async
    .run({'async': true});
