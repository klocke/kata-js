module.exports = function reorder(inputString){

    let upperCase = ''
    let lowerCase = ''
    let numbers = ''

    for (const char of [...inputString]) {

        if (/\d/.test(char)){
            numbers += char

        }
        else if (/[A-Z]/.test(char)){
            upperCase += char

        }
        else if (/[a-z]/.test(char)){
            lowerCase += char

        }
    }

    return upperCase + lowerCase + numbers
}

