import reorder from "./index";


it('should return APhpy24', function () {
    const input = "hA2p4Py"
    const expected = "APhpy24"
    expect(reorder(input)).toBe(expected)
});

it('should return MENTmove11', function () {
    const input = "m11oveMENT"
    const expected = "MENTmove11"
    expect(reorder(input)).toBe(expected)
});

it('should return OCAKEshrt94', function () {
    const input = "s9hOrt4CAKE"
    const expected = "OCAKEshrt94"
    expect(reorder(input)).toBe(expected)
});