var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
module.exports = function reorder(inputString) {
    var upperCase = '';
    var lowerCase = '';
    var numbers = '';
    for (var _i = 0, _a = __spreadArray([], inputString); _i < _a.length; _i++) {
        var char = _a[_i];
        if (/\d/.test(char)) {
            numbers += char;
        }
        else if (/[A-Z]/.test(char)) {
            upperCase += char;
        }
        else if (/[a-z]/.test(char)) {
            lowerCase += char;
        }
    }
    return upperCase + lowerCase + numbers;
};
