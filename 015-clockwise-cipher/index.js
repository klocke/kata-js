// SOLUTION, not mine
const clockwiseCipher = (message) => {
    const matLength = Math.ceil(Math.sqrt(message.length));

    let matrix = [...Array(matLength)].fill([...Array(matLength)].fill(0));
    matrix = matrix.map(x => x.slice());

    let min = 0;
    let max = matLength - 1;


    let coords = [];

    let start = 0;
    for (let s = 0; s < matLength; s++) {
        let missing = max;
        for (let i = start; i < max; i++) {
            coords.push([min, i], [i, max], [max, missing], [missing, min]);
            missing--;
        }
        start = start + 1;
        max = max - 1;
        min = min + 1;
    }

    if (matLength % 2) {
        coords.push([Math.round(matLength / 2 - 1), Math.round(matLength / 2 - 1)]);
    }

    for (let i = 0; i < matLength ** 2; i++) {
        let x = coords[i][0];
        let y = coords[i][1];
        message[i] ? matrix[x][y] = message[i] : matrix[x][y] = ' ';
    }

    let final = [];

    for (let i = 0; i < matLength; i++) {
        for (let j = 0; j < matLength; j++) {
            final.push(matrix[i][j]);
        }
    }
    return final.join('');
}



function clockwiseCipher_(message) {
    const n = Math.ceil(Math.sqrt(message.length))
    message = message.padEnd(n ** 2, ' ')
    let matrix = [...Array(n).fill([...Array(n).fill(' ')])].map(x=>x.slice())
    const max = n - 1;
    const coords = []

    for (let i = 0; i < n; i++) {
        const k = max-i
        for (let j = i; j < max; j++) {
            const l = max-j
            coords.push([i, j], [j, k], [k,l], [l, i])
        }
    }
    if (n % 2) {
        coords.push([Math.round(n / 2 - 1), Math.round(n / 2 - 1)]);
    }
    message.split('').forEach((c, i) => matrix[coords[i][0]][coords[i][1]] = c)
    return matrix.flat().join('')
}


module.exports.clockwiseCipher = clockwiseCipher_