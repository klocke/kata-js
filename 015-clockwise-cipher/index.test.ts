import {clockwiseCipher} from "./index.js"

it('should return Ms ussahr nHaaib ', function () {
    const message = "Mubashir Hassan"
    expect(clockwiseCipher(message)).toBe("Ms ussahr nHaaib")
});