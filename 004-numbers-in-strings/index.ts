/**
 * @param input is a list of strings possibly containing a number.
 *
 * regex hack /\d/ stolen from  https://stackoverflow.com/a/63546092/10074443
 * Note: regex is banned from this kata :(
 */
export default function numInString(input){
    return input.filter(val => /\d/.test(val))
}

