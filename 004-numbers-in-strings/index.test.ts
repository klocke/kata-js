import numInString from "./index";

it('should 1', function () {
    const input = ["1a", "a", "2b", "b"];
    const expected = ["1a", "2b"];

    expect(numInString(input)).toStrictEqual(expected);
});

it('should 2', function () {
    const input = ["abcd", "abc10"];
    const expected = ["abc10"];

    expect(numInString(input)).toStrictEqual(expected);
});

it('should 3', function () {
    const input = ["abc", "ab10c", "a10bc", "bcd"];
    const expected = ["ab10c", "a10bc"];

    expect(numInString(input)).toStrictEqual(expected);
});

it('should 4', function () {
    const input= ["this is a test", "test1"]
    const expected = ["test1"]
    expect(numInString(input)).toStrictEqual(expected);
});

it('should 5', function () {
    const input= ["who needs numbers","not me"]
    const expected = []
    expect(numInString(input)).toStrictEqual(expected);
});

it('should 6', function () {
    const input= ["!!", "##", "@"]
    const expected = []
    expect(numInString(input)).toStrictEqual(expected);
});