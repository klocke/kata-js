const rearrange = require("./index.js").rearrange

it('should return This is a test', function () {
    const input = "is2 Thi1s T4est 3a"

    expect(rearrange(input)).toBe("This is a Test")
});

it('should return For the good of the people', function () {
    const input = "4of Fo1r pe6ople g3ood th5e the2"
    expect(rearrange(input)).toBe("For the good of the people")
});


it('should return empty', function () {
    const input = " "
    expect(rearrange(input)).toBe("")
});