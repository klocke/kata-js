function rearrange_karl(input){
    if (input === " ") return "";
    const numberlist = []
    Array.from(input).forEach((c) => /\d/.test(c) ? numberlist.push(Number(c)-1): null)
    return input.replace(/\d+/g, '')
        .split(' ')
        .map((w, i) => [numberlist[i], w])
        .sort(([a1], [a2])=> a1-a2)
        .map(([,item]) => item)
        .reduce((p, c) =>  p+ ' ' + c)
}

function sortingFunction(a, b)  {
    const nA = a.match(/\d+/);
    const nB = b.match(/\d+/);
    return nA-nB
}

function rearrange_sebastian(input){
    return input.split(' ').sort(sortingFunction).reduce((p, c) =>  p+ ' ' + c).replace(/\d+/g, '').trim()
}



module.exports.rearrange = rearrange_sebastian
