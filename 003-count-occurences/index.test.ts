import {countOccurrences} from "./index";

it("should be one 'a' in 'edabit' ", function () {
    const input = "a";
    const word = "edabit";

    expect(countOccurrences(input, word)).toBe(1);
})


it("should be one 'c' in 'chamber of secrets' ", function () {
    const input = "c";
    const word = "Chamber of secrets";

    expect(countOccurrences(input, word)).toBe(1);
})

it("should be zero 'B' in 'boxes are fun' ", function () {
    const input = "B";
    const word = "boxes are fun";

    expect(countOccurrences(input, word)).toBe(0);
})


it("should be four 'b' in 'big fat bubble' ", function () {
    const input = "b";
    const word = "big fat bubble";

    expect(countOccurrences(input, word)).toBe(4);
})

it("should be 0 'e' in 'javascript is good' ", function () {
    const input = "e";
    const word = "javascript is good";

    expect(countOccurrences(input, word)).toBe(0);
})

it("should be zero '!' in '!easy!' ", function () {
    const input = "!";
    const word = "!easy!";

    expect(countOccurrences(input, word)).toBe(2);
})