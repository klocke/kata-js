const replaceVowels = require("./index.js").replaceVowelsReplaceAll

it('should return "k1r1ch3', function () {
    expect(replaceVowels("karAchi")).toBe("k1r1ch3")
});

it('should return "ch2mb5r', function () {
    expect(replaceVowels("chEmBur")).toBe("ch2mb5r")
});

it('should return "kh1ndb1r3', function () {
    expect(replaceVowels("khandbari")).toBe("kh1ndb1r3")
});

it('should return "l2x3c1l', function () {
    expect(replaceVowels("LexiCAl")).toBe("l2x3c1l")
});

it('should return "f5nct34ns', function () {
    expect(replaceVowels("fuNctionS")).toBe("f5nct34ns")
});

it('should return "21sy', function () {
    expect(replaceVowels("EASY")).toBe("21sy")
});