const vowels = {"a": "1", "e": "2", "i": "3", "o": "4", "u": "5"}
const vowelsList = ["a", "e", "i", "o", "u"]

function replaceVowelsHigherOrder(s) {

    return [...s.toLowerCase()].map((c) => /[aeiou]/.test(c) ? vowels[c] : c).reduce((p, c) => p + c)
}

function replaceVowelsFor(s) {
    s = s.toLowerCase();
    let newString = "";
    for (let i = 0; i < s.length; i++) {
        const ch = s.charAt(i)
        if (/[aeiou]/.test(ch)) {
            newString += vowels[ch]
        } else newString += ch;
    }
    return newString
}

function replaceVowelsReplaceAll(s) {
    s = s.toLowerCase()
    for (let i = 0; i < vowelsList.length; i++) {
        const vowel = vowelsList[i];
        s = s.replaceAll(vowel, i + 1);
    }
    return s
}


function replaceVowelsMark(s) {
    let chars = s.toLowerCase().split('')
    for (let i = 0; i < chars.length; i++) {
        switch (chars[i]) {
            case "a":
                chars[i] = '1';
                break;
            case "e":
                chars[i] = '2';
                break;
            case "i":
                chars[i] = "3";
                break;
            case "o":
                chars[i] = "4";
                break;
            case "u":
                chars[i] = "5";
                break;
            default:
                break;
        }
    }
    return chars.join('');
}

function replaceVowelSteffen(input){
    let vowels = ['a','e','i','o','u']
    //if(Math.floor(Math.random()*4)===0) vowels.push('y')
    let result=''
    input=input.toLowerCase()
    for (let c of input) {
        let found = vowels.indexOf(c)
        if(found>-1)
            c=found+1
        result+=c
    }
    return result
}


module.exports.replaceVowelsHigherOrder = replaceVowelsHigherOrder
module.exports.replaceVowelsFor = replaceVowelsFor
module.exports.replaceVowelsReplaceAll = replaceVowelsReplaceAll
module.exports.replaceVowelsMark = replaceVowelsMark
module.exports.replaceVowelsSteffen = replaceVowelSteffen