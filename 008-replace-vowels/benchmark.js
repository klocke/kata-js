const index = require("./index")
const Benchmark = require("benchmark")

const suite = new Benchmark.Suite;

// const words = "EASy"
const words = "“As he crossed toward the pharmacy at the corner" +
    " he involuntarily turned his head because of a burst of light that " +
    "had ricocheted from his temple, and saw, with that quick smile with" +
    " which we greet a rainbow or a rose, a blindingly white parallelogram " +
    "of sky being unloaded from the van—a dresser with mirrors across which," +
    " as across a cinema screen, passed a flawlessly clear reflection of boughs" +
    " sliding and swaying not arboreally, but with a human vacillation, produced " +
    "by the nature of those who were carrying this sky, these boughs, this gliding " +
    "façade.”"


// add tests
suite.add("Higher order", function () {
    index.replaceVowelsHigherOrder(words);
}).add("For loop", function () {
    index.replaceVowelsFor(words)
}).add("Replaceall", function () {
    index.replaceVowelsReplaceAll(words)
}).add("Mark's solution", function () {
    index.replaceVowelsMark(words)
}).add("Steffen's solution", function () {
    index.replaceVowelsSteffen(words)
})
    // add listeners
    .on('cycle', function (event) {
        console.log(String(event.target));
    })
    .on('complete', function () {
        console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    // run async
    .run({'async': true});