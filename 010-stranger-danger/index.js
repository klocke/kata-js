function noStranger(input){
    const words = input.split(' ');
    const unique = [... new Set(words)]
    console.log(unique)

    let aquaintances = []
    let friends = []

    for (const word of unique) {
        const res = input.split(word).length;

        console.log("Result: ")
        console.log(res);
    }

}


function noStrangerNoOrder(input){
    const acquaintances = []
    const friends = []
    const lst = [... new Set(input.split(' '))];
    lst.forEach((w, i) =>
        input.split(w).length > 5? friends.push(w) :
            input.split(w).length > 3? acquaintances.push(w):null);

    return [acquaintances, friends]
}




module.exports.noStranger = noStranger;
module.exports.noStrangerNoOrder = noStrangerNoOrder;