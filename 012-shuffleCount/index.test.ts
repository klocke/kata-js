const shuffleCount = require("./index.js").shuffleCount
const f = require("./index.js").f
import theoretically from "jest-theories";


describe("shuffleCount", function () {
    const theories = [
        {s:8, expected:3},
        {s:14, expected:12},
        {s:52, expected:8},
    ]

    theoretically(`{input} returns {expected}`, theories, theory =>  {
        const output = shuffleCount(theory.s);
        expect(output).toBe(theory.expected);
    })
})