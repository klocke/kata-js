exports.shuffleCount = function shuffleCount(number){
    const origArr = Array.from(Array(number).keys())
    let arr = origArr;
    let count = 0;
    do {
        arr = shuffle(arr)
        count++;
    }
    while (!arr.equals(origArr))
    return count
}

Array.prototype.equals = function(other){
    return this.every( (x, i) => x === other[i])
}

function shuffle(arr){
    const mid = Math.ceil(arr.length / 2)
    const a= arr.slice(0,mid)
    const b = arr.slice(mid)
    return a.flatMap((el, i) => i>=b.length? el : [el, b[i]])
}
