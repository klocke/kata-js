import {dateGender} from "./index.js"

it('should be 00T01 ', function () {
    const dob = "1/12/1900"
    const gender = "M"
    expect(dateGender(dob, gender)).toBe("00T01")

});

it('should 50T41', function () {
    const dob = "1/12/1950"
    const gender = "F"
    expect(dateGender(dob, gender)).toBe("50T41")

});