function capitalLetterFromSurname(surname) {
    const consonantRegex = /[bcdfghjklmnpqrstvwxys]/gi;
    const consonants = surname.match(consonantRegex);

    if (surname.length < 3){
        return surname.toUpperCase() + "X"
    }
    else if (consonants.length >=3){
        return consonants.reduce((p, c) => p + c).slice(0,3).toUpperCase()
    }

    else{
        const firstVowel = surname.match(/[aeiou]/i)[0].toUpperCase()

        console.log(firstVowel)
        return consonants.reduce((p, c)=> p+c).slice(0, 2).toUpperCase() + firstVowel
    }

}


function capitalLetterFromName(name) {
    const consonantRegex = /[bcdfghjklmnpqrstvwxys]/gi;
    const consonants = name.match(consonantRegex);
    const consonantNr = consonants.length;

    if (name.length<3){
        const tmp =  consonants.reduce((p, c)=> p+c).toUpperCase()
        const vowels = name.match(/[aeiou]/gi).slice(0, 3-tmp.length).reduce((p, c) =>  p + c).toUpperCase()
        return tmp + vowels + "X"
    }

    if (consonantNr===3){
        return consonants.reduce((p, c) => p + c).toUpperCase()
    }
    else if (consonantNr>3){
        return (consonants[0] + consonants[2] + consonants[3]).toUpperCase()
    }

    else if (consonantNr <3 ){
        const tmp =  consonants.reduce((p, c)=> p+c).toUpperCase()
        const vowels = name.match(/[aeiou]/gi).slice(0, 3-tmp.length).reduce((p, c) =>  p + c).toUpperCase()
        return tmp + vowels
    }
}

 function dateGender(dob, gender){
    const months = { 1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "H", 7:
            "L", 8: "M", 9: "P", 10: "R", 11: "S", 12: "T" }
    const dobList = dob.split('/')
    const yr  = String(dobList[2]).slice(-2)
    const mnth = months[Number(dobList[1])]


    let str = yr + mnth

    if (gender==="M"){
        if (Number(dobList[0]<10)) {
            str += "0" + String(dobList[0])
        }
        else{
            str += String(dobList[0])
        }
    }
    else {
        str += String(40 + Number(dobList[0]))
    }
    return str;
}


module.exports.fiscalCode = function fiscalCode(data){
    const {name, surname, gender, dob} = data;

    return capitalLetterFromSurname(surname) + capitalLetterFromName(name) + dateGender(dob, gender)

}

module.exports.capitalLetterFromSurname =  capitalLetterFromSurname
module.exports.capitalLetterFromName= capitalLetterFromName
module.exports.dateGender = dateGender
