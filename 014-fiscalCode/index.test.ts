import {fiscalCode} from "./index.js"

it('should return DBTMTT00A01', function () {

    const data = {
        name: "Matt",
        surname: "Edabit",
        gender: "M",
        dob: "1/1/1900"
    }

    expect(fiscalCode(data)).toBe("DBTMTT00A01")

});


it('should return YUXHLN50T41', function () {
    const data = {
        name: "Helen",
        surname: "Yu",
        gender: "F",
        dob: "1/12/1950"
    }

    expect(fiscalCode(data)).toBe("YUXHLN50T41")
});


it('should return MSOMKY28A16', function () {
    const data = {
        name: "Mickey",
        surname: "Mouse",
        gender: "M",
        dob: "16/1/1928"
    }

    expect(fiscalCode(data)).toBe("MSOMKY28A16")
});