import {capitalLetterFromSurname} from "./index.js"


it('should return NWM', function () {
    const surname = "Newman"
    expect(capitalLetterFromSurname(surname)).toBe("NWM")
});


it('should return FXO', function () {
    const surname = "Fox"
    expect(capitalLetterFromSurname(surname)).toBe("FXO")
});


it('should return YUX', function () {
    const surname = "Yu"
    expect(capitalLetterFromSurname(surname)).toBe("YUX")
});