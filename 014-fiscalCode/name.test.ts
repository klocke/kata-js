import {capitalLetterFromName} from "./index.js"


it('should return MTTT', function () {
    const name = "MATT"
    expect(capitalLetterFromName(name)).toBe("MTT")
});


it('should return SNT', function () {
    const name = "Samantha"
    expect(capitalLetterFromName(name)).toBe("SNT")
});


it('should return TMS', function () {
    const name = "Thomas"
    expect(capitalLetterFromName(name)).toBe("TMS")
});

it('should return BBo', function () {
    const name = "Bob"
    expect(capitalLetterFromName(name)).toBe("BBO")
});

it('should return PLA', function () {
    const name = "Paula"
    expect(capitalLetterFromName(name)).toBe("PLA")
});

it('should return LAX', function () {
    const name = "Al"
    expect(capitalLetterFromName(name)).toBe("LAX")
});