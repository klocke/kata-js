// calculate
import {basketPoints} from "./index";

it("should times 2 by 5", function () {
    // 1 assertion per test
    let points = basketPoints(5, 0)

    expect(points).toBe(10)
})


it("should times 3 by 5", function () {
    let points = basketPoints(0, 5)
    expect(points).toBe(15)
})


it("should add 2 + 3", function () {
    expect(
        basketPoints(1, 1)
    ).toBe(5)
})

it("should add seven and five shots", function () {
    expect(basketPoints(7, 5)).toBe(29)
})

it("should be 100", function () {
    expect(basketPoints(38, 8)).toBe(100)
})

it('should be 3', function () {
    expect(basketPoints(0, 1)).toBe(3)
});

it('should be 0', function () {
    expect(basketPoints(0, 0)).toBe(0)
});