import elasticWord from "./index";


it('should return ANNNNA', function () {
    const exp = "ANNNNA"
    expect(elasticWord("ANNA")).toBe(exp)
});


it('should return KAAYYYAAK', function () {
    const exp = "KAAYYYAAK"
    expect(elasticWord("KAYAK")).toBe(exp)
});

it('should return X', function () {
    const exp = "X"
    expect(elasticWord("X")).toBe(exp)
});

it('should not stretch AB', function () {
    const exp = "AB"
    expect(elasticWord(exp)).toBe(exp)
});